package com;


import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;


public class JobTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //配置
        Configuration configuration = new Configuration();
        //获取一个工作
        Job job = Job.getInstance(configuration);
        //设置工作类是哪个
        job.setJarByClass(JobTest.class);
        //设置 Mapper类和reducer类是哪个
        job.setMapperClass(MapTest.class);
        job.setReducerClass(ReduceTest.class);



        //设置map 输出key 和 value 的类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //设置 输出key 和 value 的类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);



        File file = new File("e:\\output");
        if (file.exists()){
            FileUtils.deleteDirectory(file);
        }


        //指定输入输出的路径，输出路径必须是不存在的
        FileInputFormat.setInputPaths(job, new Path("e:\\a.txt"));
        FileOutputFormat.setOutputPath(job, new Path("e:\\output"));


        //设置reducer的输出数目
        job.setNumReduceTasks(1);

        boolean re = job.waitForCompletion(true);

        System.exit(re ? 0 : 1);
    }
}
