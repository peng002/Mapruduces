package com;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

                                     //输入              //输出
public class MapTest extends Mapper<LongWritable, Text, Text, IntWritable> {


    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //读取信息
        String string = value.toString();
        //数组形式 用逗号分开
        String[] split = string.split(",");
        //遍历输出
        for (String s : split) {
            //发送                                 //eg: hello 1
            context.write(new Text(s), new IntWritable(1));
        }

    }
}
